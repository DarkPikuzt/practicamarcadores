/*Agregar imagen al marcador*/

const image =
    "https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png";



function muestra(map){

    markers = [];

    for (i = 0; i < catalogo2006_.length; i++) {
        //console.log(puntos[i])
        var position = new google.maps.LatLng(arrego_iniciativas[i]['Latitud'], arrego_iniciativas[i]['Longitud']);
        // /*Marcador*/
        let marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 20, //tamaño
                strokeColor: '#FFFFFF', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: '#0076F7', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_iniciativas[i]['folio']
        });

        markers.push(marker);




        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                let infowindow;
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth: 600
                    });
                }


                //let  informacion = 'Folio:'+ catalogo2006_[i]['folio'] ;
                infowindow.open(map, marker)
                infowindow.setContent(informacion);
                //let url = "View/MapaUUO_UV/infoConten/info.php";
                //let street = "View/MapaUUO_UV/infoConten/streetView.php";
                let  indice = arrego_iniciativas[i]['id'];
                let  lon = arrego_iniciativas[i]['Longitud'];
                let  lan = arrego_iniciativas[i]['Latitud'];

                //alert(indice)
                myFunction(indice,lon,lan);

                map.setZoom(20);
                map.setCenter(marker.getPosition());

            });
        })(i, marker);
    }

}

function myFunction(indice,lon,lan){
    let url = "Views//infoContenedorMaker/info.php";

    $.ajax({
        data: {id:indice, lon:lon,lan:lan},
        type: "POST",
        dataType: "html",
        url: url,
        success:function (data) {

            $('.aux').html(data)

            //infowindow.open(map, marker)


        }
    });


}




function iniciativa(map) {
    let markers =[];

    for (i = 0; i < arrego_PDO.length; i++) {
        var position = new google.maps.LatLng(arrego_PDO[i]['Latitud'], arrego_PDO[i]['Longitud']);
        //var tipo = iniciativa[i]['id'];


        // /*Marcador con imagen*/
        let marker = new google.maps.Marker({
            position: position,
            icon: image,
            draggable:true,
            animation: google.maps.Animation.DROP,
            map: map,
            title: arrego_PDO[i]['nombre']
        });

        markers.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                let infowindow;
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth: 600
                    });
                }


                let  informacion = 'Folio:'+ arrego_iniciativas[i]['id'] ;
                infowindow.open(map, marker)
                infowindow.setContent(informacion);
                //let url = "View/MapaUUO_UV/infoConten/info.php";
                //let street = "View/MapaUUO_UV/infoConten/streetView.php";
                let  indice = arrego_PDO[i]['id'];
                let  lon = arrego_PDO[i]['Longitud'];
                let  lan = arrego_PDO[i]['Latitud'];
                //alert(indice)
                myFunction(indice,lon,lan);

                map.setZoom(20);
                map.setCenter(marker.getPosition());

            });
        })(i, marker);






    }

    // Add a marker clusterer to manage the markers.
    var markerCluster = new MarkerClusterer(map, markers,
        {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'}
        );


}

/*creamos poligonos*/
function poligono(map) {
    // Define the LatLng coordinates for the polygon's path.
    map.data.loadGeoJson(
        "js/Poligonos/xalapa.json"
    );

    map.data.setStyle(function(feature) {
        // var ascii = feature.getProperty('ascii');
        //var color = ascii > 91 ? 'red' : 'blue';
        return {
            fillColor: 'green',
            strokeWeight: 1
        };
    });


}