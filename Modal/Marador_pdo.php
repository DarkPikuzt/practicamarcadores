<?php
include_once 'Conexion/DB_PDO.php';

class Marador_pdo
{

    public  function marcador(){
        $conexion = new DB_PDO();
        $conn = $conexion->connection();

        $sql = "       
        select
        distinct cae.idEntrevista,
        cai.Longitud,
        cai.Latitud
        from co_agr_entrevista cae 
        left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
        where cai.activo = 1   
        
        ";

        $punto = $conn->prepare($sql);
        $punto->execute();
        $result = $punto->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

}