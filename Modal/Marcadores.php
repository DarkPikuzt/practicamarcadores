<?php


/*---------------------------------------------------Iniciativas */


$sql_iniciativas = "        
     select
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
where cai.activo = 1 ;     
        ";

$result_iniciativas = $conexion->query($sql_iniciativas);


while ($row_iniciativas = $result_iniciativas->fetch_array(MYSQLI_ASSOC)) {
    $arrego_iniciativas[] = array(
        'id' => $row_iniciativas['idEntrevista'],
        'lat' => $row_iniciativas['Latitud'],
        'log' => $row_iniciativas['Longitud']
    );
}


/*valida con un if si el arreglo esta vacio o no */

if (isset($arrego_iniciativas)) {
    $arrego_iniciativas;
} else {
    $arrego_iniciativas = 0;
}

//var_dump($arrego_iniciativas);
