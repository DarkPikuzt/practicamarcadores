<?php

include_once 'Conexion/DB_Conexion.php';
include_once 'Modal/Marcadores.php';
include_once 'Controlller/cPrincipal.php';




?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mapa</title>

    <!-- Complementos a ocupar-->

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>




    <script src="https://cdnjs.cloudflare.com/ajax/libs/OverlappingMarkerSpiderfier/1.0.3/oms.min.js"></script>
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>


    <!-- JS-->
    <script src="https://unpkg.com/@google/markerclustererplus@4.0.1/dist/markerclustererplus.min.js"></script>


    <!--script type="text/javascript" src="js/graficas.js"></script-->
    <!--libreria para el sobreposicionamiento-->



    <script src="js/main-mapa.js"></script>
    <script src="js/Marcadores/marcador.js"></script>
</head>
<body>
<section class="informe-mapa">
    <style>
        #map{
            overflow:hidden;
            padding-bottom:56.25%;
            position:relative;
            width: 50%;
            height:500px;
        }
        @media (min-width: 576px) {
            #map{
                height: auto;
            }
        }
    </style>

    <!--Contentenedor en donde va el mapa-->
    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8" id="map"  >
    </div>


    <style>
        .aux{
            position: absolute;
            background-color: antiquewhite;
            z-index: 5;
            width: 300px;
            height: 300px;
            right: 0;
            top: 0;
        }

    </style>

    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 aux"  >
    </div>


</section>






</body>
<script>
    <?php
    /*llamada de  variables*/
    include_once './js/json_var.php';
    ?>
</script>





<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDKrV88GHRaVGDBAmgj2_KmGnFnFOl4zvs&callback=initMap">
</script>



</html>
